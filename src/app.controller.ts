import { Controller, Get, Post, Request, HttpStatus, Res, UseGuards } from '@nestjs/common';
import { AppService } from './app.service';
import { AuthService } from './auth/auth.service';

import {
  ApiBearerAuth,
  ApiOperation,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';
@Controller()
export class AppController {
  constructor(private readonly appService: AppService, private readonly authService: AuthService) { }

  @Get('/')
  @ApiOperation({ summary: 'Show API version' })
  @ApiResponse({ status: 200, description: 'Welcome to RNA API v0.1, go to /swagger for more information' })
  getHello(): string {
    return this.appService.getWelcomeText()
  }
}
