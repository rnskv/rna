import { Body, Controller, Post, UseFilters } from '@nestjs/common';
import { CreateUserDto } from 'src/users/dto/create-user.dto';
import { UsersService } from 'src/users/users.service';
import { MongoExceptionFilter } from 'src/database/mongo-exception.filter';

import {
    ApiBearerAuth,
    ApiOperation,
    ApiResponse,
    ApiTags,
} from '@nestjs/swagger';
@Controller('registration')
export class RegistrationController {
    constructor(private readonly usersService: UsersService) { }

    @Post('/local')
    @UseFilters(MongoExceptionFilter)
    @ApiOperation({ summary: 'Register user by login, password' })
    @ApiBearerAuth("token")
    @ApiResponse({ status: 200, description: 'Return registered user' })
    registrationLocal(@Body() createUserDto: CreateUserDto) {
        const { photo, ...userData } = createUserDto;

        return this.usersService.create({
            ...userData,
            photo: photo || `https://api.adorable.io/avatars/70/${userData.login}`
        })
    }

    @Post('/vk')
    @ApiOperation({ summary: 'Register user by vk' })
    @ApiBearerAuth("token")
    @ApiResponse({ status: 200, description: 'Return registered user' })
    registrationVk(@Body() createUserDto: CreateUserDto) {
        const { photo, ...userData } = createUserDto;

        try {
            return this.usersService.create({
                ...userData,
                photo: photo || `https://api.adorable.io/avatars/100/${userData.login}`
            })
        } catch (error) {
            return error
        }
    }
}
