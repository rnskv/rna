import { Module } from '@nestjs/common';
import { RegistrationController } from './registration.controller';
import { UsersService } from '../users/users.service';
import { UsersModule } from 'src/users/users.module';

@Module({
    imports: [UsersModule],
    controllers: [RegistrationController],
    providers: [],
})
export class RegistrationModule {

}
