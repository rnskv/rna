import { Controller, Get, Body, Param, Query, UseGuards, Request, Put, UnprocessableEntityException } from '@nestjs/common';
import { UsersService } from './users.service';
import { JwtAuthGuard } from '../auth/jwt-auth.guard';

import {
    ApiBearerAuth,
    ApiOperation,
    ApiResponse,
    ApiTags,
} from '@nestjs/swagger';
@Controller('users')
export class UsersController {
    constructor(private readonly usersService: UsersService) { }

    @Get('/')
    @ApiOperation({ summary: 'Get all users' })
    @ApiBearerAuth("token")
    @ApiResponse({ status: 200, description: 'Return all users list' })
    getAll() {
        return this.usersService.getAll();
    }

    @UseGuards(JwtAuthGuard)
    @Get('/profile')
    @ApiOperation({ summary: 'Get user profile' })
    @ApiBearerAuth("token")
    @ApiResponse({ status: 200, description: 'User profile' })
    async getProfile(@Request() req) {
        const profile = await this.usersService.getByLogin(req.user.login);
        console.log('profile', profile)
        if (!profile) {
            throw new UnprocessableEntityException("Deleted user");
        }
        return profile
    }

    @Get('/list')
    @ApiOperation({ summary: 'Get users list' })
    @ApiResponse({ status: 200, description: 'User list' })
    async getUsersList(@Query() query, @Request() req, @Body() body) {
        const { page, numsOnPage, exclude = [], sortBy, sortRule } = query;
        console.log('Get users list', query)
        return this.usersService.getList({ page: Number(page), sortBy, sortRule, numsOnPage: Number(numsOnPage), exclude: typeof exclude === 'string' ? [exclude] : exclude });
    }

    @Get('/:id')
    @ApiOperation({ summary: 'Get user by id' })
    @ApiBearerAuth("token")
    @ApiResponse({ status: 200, description: 'Return user with given id' })
    async getOne(@Param() params: any) {
        const user = await this.usersService.getById(params.id);;
        return user
    }

    @UseGuards(JwtAuthGuard)
    @Put('/salary')
    @ApiOperation({ summary: 'Change user salary' })
    @ApiBearerAuth("token")
    @ApiResponse({ status: 200, description: 'User profile' })
    async changeSalary(@Request() req, @Body() body) {
        const { salary } = body;

        return this.usersService.updateByLogin(req.user.login, { salary });
    }
}
