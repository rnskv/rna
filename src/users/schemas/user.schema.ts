import * as mongoose from 'mongoose';

export const UserSchema = new mongoose.Schema({
  login: {
    type: String,
    required: true,
    unique: true,
    index: true
  },
  password: {
    type: String,
  },
  email: {
    type: String
  },
  name: {
    type: String,
    required: true,
  },
  lastname: {
    type: String,
    required: true,
  },
  age: {
    type: Number,
  },
  salary: {
    type: Number,
    required: true,
  },
  photo: {
    type: String,
  },
  vkId: {
    type: Number,
  }
});
