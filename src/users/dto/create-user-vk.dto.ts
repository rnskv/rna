import { IsNotEmpty } from 'class-validator';

export class CreateUserVkDto {
    vkId: number;
    name: string;
    login: string;
    lastname: string;
    salary: number;    
    photo: string;
}