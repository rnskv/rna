import { IsNotEmpty } from 'class-validator';

export class CreateUserDto {
    @IsNotEmpty()
    name: string;

    @IsNotEmpty()
    login: string;

    @IsNotEmpty()
    email: string;

    @IsNotEmpty()
    password: string;

    @IsNotEmpty()
    lastname: string;

    @IsNotEmpty()
    salary: number;
    
    @IsNotEmpty()
    age: number;

    photo: string;
}