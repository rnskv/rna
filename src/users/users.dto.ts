export class UsersDto {
    name: string;
    login: string;
    email: string;
    password: string;
    lastname: string;
    salary: number;
    age: number;
}