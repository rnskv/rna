import { Model, Types } from 'mongoose';
import { Injectable, Inject } from '@nestjs/common';
import { User } from './interfaces/user';
import { CreateUserDto } from './dto/create-user.dto';
import { CreateUserVkDto } from './dto/create-user-vk.dto';

@Injectable()
export class UsersService {
    constructor(@Inject('USER_MODEL') private userModel: Model<User>) { }
    getAll() {
        const allUsers = this.userModel.find();
        return allUsers
    }
    getList({
        exclude = [],
        page = 1,
        numsOnPage = 5,
        sortBy = "_id",
        sortRule = -1 
    }: {
        sortBy: string,
        sortRule: number,
        exclude: any,
        page: number,
        numsOnPage: number,
    }) {
        console.log('sort', sortBy, sortRule)
        const users = this.userModel
            .find({ _id: { $nin: exclude.map((id: string) => Types.ObjectId(id)) } })
            .sort({ [sortBy]: sortRule})
            .limit(numsOnPage)
            .skip(numsOnPage * (page - 1));
        return users;
    }
    getById(id: string) {
        return this.userModel.findOne({ _id: id });
    }
    getByLogin(login: string) {
        return this.userModel.findOne({ login })
    }

    create(createUserDto: CreateUserDto) {
        const createdUser = new this.userModel(createUserDto)
        return createdUser.save();
    }

    createByVk(createUserDto: CreateUserVkDto) {
        const createdUser = new this.userModel(createUserDto)
        return createdUser.save();
    }

    getByEmail(email: string) {
        return this.userModel.findOne({ email })

    }

    getByVkId(vkId: number) {
        return this.userModel.findOne({ vkId })
    }

    updateByLogin(login: string, diff: any) {
        return this.userModel.updateOne({ login }, diff)
    }
}
