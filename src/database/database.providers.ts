import * as mongoose from 'mongoose';

export const databaseProviders = [
  {
    provide: 'DATABASE_CONNECTION',
    useFactory: (): Promise<typeof mongoose> => {
      return mongoose.connect('mongodb://admin:admin1@ds263295.mlab.com:63295/rna_dev')
    }
  },
];