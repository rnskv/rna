import { ArgumentsHost, Catch, ConflictException, ExceptionFilter } from '@nestjs/common';
import { MongoError } from 'mongodb';

@Catch(MongoError)
export class MongoExceptionFilter implements ExceptionFilter {
    catch(exception: MongoError, host: ArgumentsHost) {
        const response = host.switchToHttp().getResponse();
        if (exception.code === 11000) {
            response.status(400).json({ message: 'Имя пользователя либо email заняты' });
        } else {
            response.status(500).json({ message: 'Internal error.' });
        }
    }
}