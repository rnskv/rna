import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { UsersModule } from './users/users.module';
import { AuthModule } from './auth/auth.module';
import { RegistrationModule } from './registration/registration.module';
import { SalaryModule } from './salary/salary.module';
import { ConfigModule } from '@nestjs/config';

@Module({
  imports: [UsersModule, AuthModule, RegistrationModule, SalaryModule, ConfigModule.forRoot({ envFilePath: ['.env.development', '.env'] })],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule { }
