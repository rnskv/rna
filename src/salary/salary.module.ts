import { Module } from '@nestjs/common';
import { SalaryController } from './salary.controller';
import { SalaryService } from './salary.service';
import { DatabaseModule } from '../database/database.module';
import { salaryProviders } from './salary.providers';

@Module({
  imports: [DatabaseModule],
  controllers: [SalaryController],
  providers: [SalaryService, ...salaryProviders]
})
export class SalaryModule {}
