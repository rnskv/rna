import { Document } from 'mongoose';

export interface Salary extends Document {
  readonly ownerId: string;
  readonly value: number;
  readonly currency: string;
}
