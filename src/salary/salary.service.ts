import { Injectable, Inject } from '@nestjs/common';
import { Model } from 'mongoose';
import { Salary } from './interfaces/salary';
import { CreateSalaryDto } from './dto/create-salary.dto';

@Injectable()
export class SalaryService {
    constructor(@Inject('SALARY_MODEL') private salaryModel: Model<Salary>) { }
    getByOwnerId(ownerId: string) {
        return this.salaryModel.find({ ownerId })
    }

    create(createSalaryDto: CreateSalaryDto) {
        const newSalary = new this.salaryModel(createSalaryDto);;
        return newSalary.save();
    }
}
