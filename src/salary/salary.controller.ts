import { Controller, Body, Get, Param, Post, UseGuards, Request } from '@nestjs/common';
import { SalaryService } from './salary.service';
import { JwtAuthGuard } from '../auth/jwt-auth.guard';
import { CreateSalaryDto } from './dto/create-salary.dto';


@Controller('salary')
export class SalaryController {
    constructor(private readonly salaryService: SalaryService) { }
    @Get('/owner/:id')
    @UseGuards(JwtAuthGuard)
    getSalaryByOwnerId(@Param() params) {
        return this.salaryService.getByOwnerId(params.id)
    }

    @Post('/')
    @UseGuards(JwtAuthGuard)
    addSalary(@Request() req, @Body() createSalaryDto: CreateSalaryDto) {
        return this.salaryService.create({
            ownerId: req.user._id,
            ...createSalaryDto
        });
    }
}
