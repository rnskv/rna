import { Connection } from 'mongoose';
import { SalarySchema } from './schemas/salary.schema';

export const salaryProviders = [
  {
    provide: 'SALARY_MODEL',
    useFactory: (connection: Connection) => connection.model('Salary', SalarySchema),
    inject: ['DATABASE_CONNECTION'],
  },
];