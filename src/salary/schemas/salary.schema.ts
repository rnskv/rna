import * as mongoose from 'mongoose';
const Schema = mongoose.Schema;

export const SalarySchema = new mongoose.Schema({
  ownerId: {
      type: Schema.Types.ObjectId,
  },
  value: Number,
  currency: String,
});
