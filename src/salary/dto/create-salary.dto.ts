import { IsNotEmpty } from 'class-validator';

export class CreateSalaryDto {
    ownerId: string;

    @IsNotEmpty()
    value: number;

    @IsNotEmpty()
    currency: string;
}