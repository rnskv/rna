import { Injectable, HttpService } from '@nestjs/common';
import { UsersService } from '../users/users.service';
import { JwtService } from '@nestjs/jwt';

@Injectable()
export class AuthService {
    constructor(private usersService: UsersService, private jwtService: JwtService, private http: HttpService) { }
    async validateUser(login: string, password: string) {
        const user = await this.usersService.getByLogin(login);
        console.log('validate user', login, password, user)
        if (user && user.password === password) {
            return user;
        }
        return null;
    }

    async login(user: any) {
        console.log('login', user)
        const payload = { login: user.login, _id: user._id };

        return {
            token: this.jwtService.sign(payload),
        };
    }

    async getVkToken(code: string): Promise<any> {
        const { VK_APP_ID, VK_APP_SECRET, FRONTEND_HOST } = process.env;

        return this.http
            .get(
                `https://oauth.vk.com/access_token?client_id=${VK_APP_ID}&client_secret=${VK_APP_SECRET}&redirect_uri=${FRONTEND_HOST}/auth/login&code=${code}`
            )
            .toPromise();
    }

    async getUserDataFromVk(userId: string, token: string): Promise<any> {
        return this.http
            .get(
                `https://api.vk.com/method/users.get?user_ids=${userId}&fields=photo_400,has_mobile,home_town,contacts,mobile_phone&access_token=${token}&v=5.120`
            )
            .toPromise();
    }
}
