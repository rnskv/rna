import { Controller, Post, Request, UseGuards, ValidationPipe, UnprocessableEntityException, Body, UseFilters } from '@nestjs/common';
import { LocalAuthGuard } from './local-auth.guard';
import { MongoExceptionFilter } from 'src/database/mongo-exception.filter';
import { AuthService } from './auth.service';
import {
    ApiBearerAuth,
    ApiOperation,
    ApiResponse,
    ApiTags,
} from '@nestjs/swagger';
import { UsersService } from 'src/users/users.service';
@Controller('auth')
export class AuthController {
    constructor(private readonly authService: AuthService, private usersService: UsersService) { }

    @UseGuards(LocalAuthGuard)
    @Post('/login')
    @ApiOperation({ summary: 'Login user' })
    @ApiBearerAuth("token")
    @ApiResponse({ status: 200, description: 'Return user jwt' })

    async login(@Request() req) {
        return this.authService.login(req.user);
    }

    @Post('/login/vk')
    @ApiOperation({ summary: 'Login user by vk' })
    @UseFilters(MongoExceptionFilter)
    async vk(@Body(new ValidationPipe()) auth: { code: string }) {
        let authData;

        try {
            authData = await this.authService.getVkToken(auth.code);
        } catch (err) {
            console.log('/login/vk error', err)
            throw new UnprocessableEntityException("Wrong VK code");
        }

        const _user = await this.usersService.getByVkId(authData.data.user_id);

        if (_user) {
            console.log('user login by vk');
            return this.authService.login({ login: _user.login });
        }

        try {
            const { data } = await this.authService.getUserDataFromVk(
                authData.data.user_id,
                authData.data.access_token
            );

            const [profile] = data.response;

            await this.usersService.createByVk({
                login: `uid_${profile.id}`,
                name: profile.first_name,
                lastname: profile.last_name,
                photo: profile.photo_400,
                salary: 0,
                vkId: profile.id,
            });

            console.log('user registration by vk');
            return this.authService.login({ login: `uid_${profile.id}` });
        } catch (err) {
            console.log(err)
            // throw new UnprocнessableEntityException(err);
        }
    }
}
