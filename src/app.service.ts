import { Injectable } from '@nestjs/common';

@Injectable()
export class AppService {
  getWelcomeText(): string {
    return 'Welcome to RNA API v0.1, go to /swagger for more information. VK_APP - ' + JSON.stringify(process.env.VK_APP_ID)
  }
  getHello(): string {
    return 'Hello World!!!';
  }
}
